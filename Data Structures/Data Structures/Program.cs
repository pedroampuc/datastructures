﻿using System;

namespace Data_Structures
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Min heap
            /*MinHeap h = new MinHeap(11);
            h.insertKey(3);
            h.insertKey(2);
            h.deleteKey(1);
            h.insertKey(15);
            h.insertKey(5);
            h.insertKey(4);
            h.insertKey(45);

            Console.WriteLine(h.extractMin() + " ");
            Console.WriteLine(h.getMin() + " ");

            h.decreaseKey(2, 1);
            Console.WriteLine(h.getMin());*/
            #endregion

            #region Binary Tree
            BinaryTree tree = new BinaryTree();

            /* Let us create following BST
                  50
               /     \
              30      70
             /  \    /  \
           20   40  60   80 */

            tree.insert(50);
            tree.insert(30);
            tree.insert(20);
            tree.insert(40);
            tree.insert(70);
            tree.insert(60);
            tree.insert(80);

            // Print inorder traversal of the BST
            tree.inorder();

            Console.WriteLine("Searching for 70...");
            Node n = tree.search(tree.root, 70);
            Console.WriteLine("node found is {0}, child.left: {1}, child.right: {2}", n.key, n.left, n.right);

            Console.WriteLine("Searching for 60...");
            n = tree.search(tree.root, 60);
            Console.WriteLine("node found is {0}, child.left: {1}, child.right: {2}", n.key, n.left, n.right);

            Console.WriteLine("Tree height: {0}", tree.maxDepth(tree.root));
            Console.WriteLine("End");
            #endregion

        }
    }
}
