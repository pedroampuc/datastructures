﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Structures
{
    // Class containing left and 
    // right child of current node
    // and key value
    public class Node
    {
        public int key;
        public Node left, right;

        public Node(int item)
        {
            key = item;
            left = right = null;
        }
    }
    // Binary SEARCH!!!!!! tree
    class BinaryTree
    {
        // Root of BST
        public Node root;

        // Constructor
        public BinaryTree()
        {
            root = null;
        }

        // This method mainly calls insertRec()
        public void insert(int key)
        {
            root = insertRec(root, key);
        }

        // A recursive function to insert 
        // a new key in BST 
        public Node insertRec(Node root, int key)
        {

            // If the tree is empty, 
            // return a new node 
            if (root == null)
            {
                root = new Node(key);
                return root;
            }

            // Otherwise, recur down the tree 
            if (key < root.key)
                root.left = insertRec(root.left, key);
            else if (key > root.key)
                root.right = insertRec(root.right, key);

            // Return the (unchanged) node pointer 
            return root;
        }

        // This method mainly calls InorderRec()
        public void inorder()
        {
            inorderRec(root);
        }

        // A utility function to 
        // do inorder traversal of BST
        void inorderRec(Node root)
        {
            if (root != null)
            {
                inorderRec(root.left);
                Console.WriteLine(root.key);
                inorderRec(root.right);
            }
        }

        // A utility function to search 
        // a given key in BST
        public Node search(Node root,
                           int key)
        {
            // Base Cases: root is null 
            // or key is present at root
            if (root == null ||
                root.key == key)
                return root;

            // Key is greater than root's key
            if (root.key < key)
                return search(root.right, key);

            // Key is smaller than root's key
            return search(root.left, key);
        }

        /* Compute the "maxDepth" of a tree -- the number of
        nodes along the longest path from the root node
        down to the farthest leaf node.*/
        public int maxDepth(Node node)
        {
            if (node == null)
                return -1;
            else
            {
                /* compute the depth of each subtree */
                int lDepth = maxDepth(node.left);
                int rDepth = maxDepth(node.right);

                /* use the larger one */
                return (1 + Math.Max(lDepth, rDepth));

            }
        }

        // This method mainly calls deleteRec()
        void deleteKey(int key) { root = deleteRec(root, key); }

        /* A recursive function to
          delete an existing key in BST
         */
        Node deleteRec(Node root, int key)
        {
            /* Base Case: If the tree is empty */
            if (root == null)
                return root;

            /* Otherwise, recur down the tree */
            if (key < root.key)
                root.left = deleteRec(root.left, key);
            else if (key > root.key)
                root.right = deleteRec(root.right, key);

            // if key is same as root's key, then This is the
            // node to be deleted
            else
            {
                // node with only one child or no child
                if (root.left == null)
                    return root.right;
                else if (root.right == null)
                    return root.left;

                // node with two children: Get the
                // inorder successor (smallest
                // in the right subtree)
                root.key = minValue(root.right);

                // Delete the inorder successor
                root.right = deleteRec(root.right, root.key);
            }
            return root;
        }

        int minValue(Node root)
        {
            int minv = root.key;
            while (root.left != null)
            {
                minv = root.left.key;
                root = root.left;
            }
            return minv;
        }

    }
}

